""" Python Log Parser - Alex Pulschen """
import getopt
import sys


def main(argv):
    """ Main routine. Parses args and options, then loops through each line of each file and
        sorts it by domain.

        Global variables:
            start_date      Accepts epoch time.
            end_date        Accepts epoch time.
            file_list       List of files to parse.
            line_count      Dictionary used to count total lines per domain.
            error_count     Dictionary used to count 5xx errors per domain.
    """
    start_date = 0
    end_date = 0
    file_list = []
    line_count = {}
    error_count = {}
    help_string = "This tool\'s purpose is to parse web server logs. At least one log file is " \
                  "required. \n\nUsage: parse.py -s <epoch_time> -e <epoch_time> log1.txt " \
                  "log2.txt...\n\n     Arguments:" \
                  "\n           -s, --start=        The start time in epoch to search." \
                  "\n           -e, --end=          The end time in epoch to search." \
                  "\n           -h                  Show help and usage." \
                  "\n           [file1] [file2]     Accepts 1 or more files to parse through. " \
                  "Separated by space."

    # Check if required args exist. Add file(s) to list.
    try:

        if len(sys.argv) <= 5:
            print(help_string)
            sys.exit(2)

        opts, args = getopt.getopt(argv, "hs:e:", ["start=", "end="])
        arg_count = 5

        while arg_count < len(sys.argv):
            file_list.append(sys.argv[arg_count])
            arg_count += 1

    except getopt.GetoptError as err:
        print(err, help_string)
        sys.exit(2)

    # Grab args from options and set them
    for opt, arg in opts:

        if opt == '-h':
            print(help_string)
            sys.exit()
        elif opt in ("-s", "--start"):
            start_date = arg
        elif opt in ("-e", "--end"):
            end_date = arg

    # For each log file, open and read through lines.
    for log in file_list:

        try:
            with open(log) as file:
                file = file.readlines()

            # Split the line and grab what we need to evaluate.
            for line in file:
                full_line = line.split(' | ')
                resp_time = full_line[0]
                resp_domain = full_line[2]
                resp_code = full_line[4]

                # If the date is within range, check if the response is a 500. If so, count the
                # error and domain using the error_count dictionary. Regardless, count the amount of
                # lines per domain.
                if start_date <= resp_time < end_date:
                    if resp_code[0] == '5':
                        if resp_domain not in error_count.keys():
                            error_count[resp_domain] = 1
                        else:
                            error_count[resp_domain] += 1
                    if resp_domain not in line_count.keys():
                        line_count[resp_domain] = 1
                    else:
                        line_count[resp_domain] += 1
        except IOError:
            print('Could not open file.')

    # Output in the format specified
    print('Between time ', start_date, ' and time', end_date, ':')

    # If the domain has errors, count the percentage of errors that the domain received
    # and output
    for domain in error_count:
        err_percent = error_count[domain] / line_count[domain] * 100
        print(domain, 'returned ', round(err_percent, 2), '% 5xx errors')


if __name__ == "__main__":
    main(sys.argv[1:])
